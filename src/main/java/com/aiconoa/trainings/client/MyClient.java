package com.aiconoa.trainings.client;

import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Form;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.aiconoa.trainings.entity.Event;
import com.aiconoa.trainings.entity.Subscriber;

public class MyClient {

    public static void main(String[] args) {
        String token = authentification("admin", "admin");
        displayListFromAuthor(token);
        // confirm(2,"CECFE6A6796D703A77B47199AC870B9D6719BDB2");
    }

    public static void displayList(String token) {
        List<Subscriber> subscribers = new ArrayList<Subscriber>();
        Client client = ClientBuilder.newClient();
        subscribers = client.target("http://localhost:8080/event/api/event/listByUser/jeremypansier@gmail.com")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", token)
                .get(new GenericType<List<Subscriber>>() {
                });
        for (Subscriber subscriber : subscribers) {
            System.out.print(subscriber.getEvent().getTitle() + " : ");
            System.out.println(subscriber.getEvent().getDescription());
            System.out.println("hashCode : " + subscriber.getHashCode());
            System.out.println();
        }
        client.close();
    }
    public static void displayListFromAuthor(String token) {
        List<Event> events = new ArrayList<Event>();
        Client client = ClientBuilder.newClient();
        events = client.target("http://localhost:8080/event/api/event/list")
                .request(MediaType.APPLICATION_JSON)
                .header("Authorization", "Bearer "+token)
                .get(new GenericType<List<Event>>() {
                });
        for (Event event : events) {
            System.out.print(event.getTitle() + " : ");
            System.out.println(event.getDescription());
            System.out.println();
        }
        client.close();
    }
    
    public static void confirm(int choice, String hashCode) {
        Client client = ClientBuilder.newClient();

        client.target("http://localhost:8080/event/api/event/confirm/" + hashCode).request()
                .accept(MediaType.APPLICATION_JSON).put(Entity.json(choice));

        client.close();
    }

    public static String authentification(String username, String password) {

        Form input = new Form();
        input.param("username", username);
        input.param("password", password);
        Entity<Form> entity = Entity.entity(input, MediaType.APPLICATION_FORM_URLENCODED);

        Client client = ClientBuilder.newClient();

        Response response = client.target("http://localhost:8080/event/api/event/authentification")
                .request(MediaType.APPLICATION_JSON)
                .accept(MediaType.APPLICATION_FORM_URLENCODED)
                .post(entity);

        return response.readEntity(String.class);
    }

}
