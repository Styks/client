package com.aiconoa.trainings.entity;

public class User {
	private int idUser;
	private String email;
	
	public User() {
        super();
    }

    public User(int idUser, String email) {
		super();
		this.idUser = idUser;
		this.email = email;
	}
	
	public User(int idUser) {
        this.idUser = idUser;
    }

    public int getIdUser() {
		return idUser;
	}
	
	public String getEmail() {
		return email;
	}

	public void setIdUser(int idUser) {
		this.idUser = idUser;
	}

	public void setEmail(String email) {
		this.email = email;
	}
}
