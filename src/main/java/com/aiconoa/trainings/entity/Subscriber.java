package com.aiconoa.trainings.entity;

import java.util.Date;


public class Subscriber {

    private int id;
    private String hashCode;
    private int mailStatus;
    private String email;
    private int isPresent;
    private String isPresentString;
    private Date sendingDate;
    private String sendingDateString;
    private String iconeLink;
    private int idEvent;
    private int idUser;

    private User user;
    private Event event;

    public Subscriber() {
        super();
    }

    public Subscriber(int idEvent, int idUser) {
        super();
       // this.event = new Event(idEvent);
        this.user = new User(idUser);
    }

    public void setHashCode(String hashCode) {
        this.hashCode = hashCode;
    }

    public String getIconeLink() {
        return iconeLink;
    }

    public String getEmail() {
        return user.getEmail();
    }

    public String getSendingDateString() {
        return sendingDateString;
    }

    public int getIsPresent() {
        return isPresent;
    }

    public void setIsPresent(int isPresent) {
        this.isPresent = isPresent;
    }

    public String getIsPresentString() {
        return isPresentString;
    }

   // public int getIdEvent() {
   //     return event.getIdEvent();
    //}

  //  public void setIdEvent(int idEvent) {
  //      this.event = new Event(idEvent);
  //  }

//    public void setIdUser(int idUser) {
//        this.user = new User(idUser);
//    }
//
//    public int getIdUser() {
//        return user.getIdUser();
//    }

    public String getHashCode() {
        return hashCode;
    }

    public int getMailStatus() {
        return mailStatus;
    }

    public void setMailStatus(int mailStatus) {
        this.mailStatus = mailStatus;
    }

    public Date getSendingDate() {
        return sendingDate;
    }

    public int getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Event getEvent() {
        return event;
    }

    public void setEvent(Event event) {
        this.event = event;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setIsPresentString(String isPresentString) {
        this.isPresentString = isPresentString;
    }

    public void setSendingDate(Date sendingDate) {
        this.sendingDate = sendingDate;
    }

    public void setSendingDateString(String sendingDateString) {
        this.sendingDateString = sendingDateString;
    }

    public void setIconeLink(String iconeLink) {
        this.iconeLink = iconeLink;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public int getIdUser() {
        return idUser;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }
}
