package com.aiconoa.trainings.entity;


public class Event {
    private static final String LINKTOEVENTLINKSERVLET = "eventLink.xhtml?id=";
    private int idEvent;
    private String title;
    private String description;
    private String linkToEventLinkServlet;
    private String fileName;
    private Author author;

    public Event() {
        super();
    }

    public Event(int idEvent) {
        this.idEvent = idEvent;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public String getLinkToEventLinkServlet() {
        return linkToEventLinkServlet;
    }

    public int getIdEvent() {
        return idEvent;
    }

    public void setIdEvent(int idEvent) {
        this.idEvent = idEvent;
    }

    public Author getAuthor() {
        return author;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static String getLinktoeventlinkservlet() {
        return LINKTOEVENTLINKSERVLET;
    }

    public void setLinkToEventLinkServlet(String linkToEventLinkServlet) {
        this.linkToEventLinkServlet = linkToEventLinkServlet;
    }

    public void setAuthor(Author author) {
        this.author = author;
    }
}
